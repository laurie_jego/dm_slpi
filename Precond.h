#include "Eigen"
#include "Sparse"
#include "Dense"
// #include "MatrixDefinition.h"
#include <fstream>
#include <iostream>

class Precond
{
protected:
  int _N;
  Eigen::VectorXd _x, _b;
  Eigen::SparseMatrix<double> _A, _L, _R; //M=L*R

public:
  Precond();

  virtual void Decomposition(Eigen::SparseMatrix<double> A)=0;
  void ResolutionLeft(Eigen::SparseMatrix<double> L, Eigen::VectorXd b);
  void ResolutionRight(Eigen::SparseMatrix<double> R, Eigen::VectorXd b);
  const Eigen::SparseMatrix<double> & GetLeftMatrix() const;
  const Eigen::SparseMatrix<double> & GetRightMatrix() const;
  const Eigen::VectorXd & GetSolutionLU() const;
};

///////////////////////////
//Gauss-Seidel symetrique//
///////////////////////////
class GaussSeidelSym : public Precond
{
public:
  void Decomposition(Eigen::SparseMatrix<double> A);
};

//////////////////////
//Cholesky incomplet//
//////////////////////
class CholeskyIncomplet : public Precond
{
public:
  void Decomposition(Eigen::SparseMatrix<double> A);
};

///////////////////////
//No preconditionneur//
///////////////////////
class NoPrecond : public Precond
{
public:
  void Decomposition(Eigen::SparseMatrix<double> A);
};
