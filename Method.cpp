#include "Method.h"
#include <iostream>
#include <fstream>
#include <vector>

using namespace Eigen;
using namespace std;


///////////////////////////
//Constructeur par défaut//
///////////////////////////
//Constructeur
Method::Method(): _precond(0)
{

}
//SaveSolution
void Method::SaveSolution(const std::string file_name)
{
  _fileout.open(file_name);
  for (int i = 0; i < _x.size(); i++)
  {
    _fileout << _x[i] << endl;;}
  }


  //Choix preconditionneur
void Method::PrecondChoice(Precond* precond)
{
  _precond = precond;
}


  //////////////////////////
  //Gradient à pas optimal//
  //////////////////////////
  void GradientPasOptimal::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    //Initialisation
    _N = N;
    _A.resize(_N,_N);
    _b.resize(_N);
    _A = A;
    cout << "A.rows = " << _A.rows() << endl;
    _b = b;
    _r.resize(_N);
    _x.resize(_N);

    cout << "taille x0" << endl;
    _x0.resize(_N);
    cout << _x0.size() << endl;
    cout << "x0" << endl;
    for (int i = 0; i < _N; i++)
      _x0[i] = i;
    cout << _x0 << endl;

    cout << "x" <<endl;
    _x = _x0;
    cout << _x << endl;
    cout << "r" << endl;
    cout << "A.rows = " << _A.rows() << endl;
    cout << "_r.size = " << _r.size() << endl;
    cout << "_b.size = " << _b.size() << endl;
    cout << " x.size = " << _x.size() << endl;
    _r = _b - _A*_x;
    cout << _r << endl;

    _normr = 0;

    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 4*_N;

    //Résolution
    VectorXd z;
    z.resize(_N);
    double alpha;
    int k(0);

    while((_normr > _eps) && (k <= _kmax))
    {
      z = _A*_r;
      alpha = (_r.dot(_r)) / (z.dot(_r));
      _x += alpha*_r;
      _r += -alpha*z;

      for (int i = 0; i < _N; i++)
      {
        _normr += _r[i]*_r[i];
      }
      _normr = sqrt(_normr);
      _vectnormr.push_back(_normr);
      // _vectnbiteration.push_back(k);
      k++;
    }
    _nbiteration = k;
    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << "  " << k << endl;
    }

    for (int i=0; i<N; i++){
      cout << "vectnormr" << endl;
      cout << _vectnormr[i] << endl;}
      // cout << "vectnbiteration" << endl;
      // cout << _vectnbiteration[i] << endl;}
  }

  //////////////////
  //Résidu minimum//
  //////////////////
  void ResiduMinimum::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    //Initialisation
    // cout << "N" << endl;
    _N = N;
    // cout << _N << endl;
    _A.resize(_N,_N);
    _b.resize(_N);
    // cout << "A" << endl;
    _A = A;
    // cout << _A << endl;
    _b = b;
    _r.resize(_N);
    _x.resize(_N);

    // cout << "taille x0" << endl;
    _x0.resize(_N);
    // cout << _x0.size() << endl;
    // cout << "x0" << endl;
    for (int i = 0; i < _N; i++)
      _x0[i] = i;
    // cout << _x0 << endl;

    // cout << "x" <<endl;
    _x = _x0;
    // cout << _x << endl;
    // cout << "r" << endl;
    _r = _b - _A*_x;
    // cout << _r << endl;

    _normr = 0;

    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = _N*_N;

    //Résolution
    VectorXd z;
    z.resize(_N);
    double alpha;
    int k(0);

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = _A*_r;
      alpha = (_r.dot(z)) / (z.dot(z));
      _x += alpha*_r;
      _r += -alpha*z;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      _vectnormr.push_back(_normr);
      _vectnbiteration.push_back(k);
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }

  // /////////////////////
  // //Gradient conjugué//
  // /////////////////////
  void GradientConjugue::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    //Initialisation
    // cout << "N" << endl;
    _N = N;
    // cout << _N << endl;
    _A.resize(_N,_N);
    _b.resize(_N);
    // cout << "A" << endl;
    _A = A;
    // cout << _A << endl;
    _b = b;
    _r.resize(_N);
    _x.resize(_N);

    // cout << "taille x0" << endl;
    _x0.resize(_N);
    // cout << _x0.size() << endl;
    // cout << "x0" << endl;
    for (int i = 0; i < _N; i++)
      _x0[i] = i;
    // cout << _x0 << endl;

    // cout << "x" <<endl;
    _x = _x0;
    // cout << _x << endl;
    // cout << "r" << endl;
    _r = _b - _A*_x;
    // cout << _r << endl;

    _normr = 0;

    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 4*_N;

    //Résolution
    VectorXd p, z, r1;
    double alpha, gamma;
    int k(0);
    p = _r;

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = _A*p;
      alpha = (_r.dot(_r)) / (z.dot(p));
      _x = _x + alpha*p;
      r1 = _r - alpha*z;
      gamma = (r1.dot(r1)) / (_r.dot(_r));
      p = r1 + gamma*p;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      _r = r1;
      _vectnormr.push_back(_normr);
      _vectnbiteration.push_back(k);
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }

  //////////////////////////////////////////
  //Résidu minimum preconditionne a gauche//
  //////////////////////////////////////////
  void ResMinPG::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    double alpha;
    int k(0);
    //Initialisation
    // cout << "N" << endl;
    _N = N;
    // cout << _N << endl;
    _A.resize(_N,_N);
    _b.resize(_N);
    // cout << "A" << endl;
    _A = A;
    // cout << _A << endl;
    _b = b;
    _r.resize(_N);
    _x.resize(_N);

    // cout << "taille x0" << endl;
    _x0.resize(_N);
    // cout << _x0.size() << endl;
    // cout << "x0" << endl;
    for (int i = 0; i < _N; i++)
      _x0[i] = i;
    // cout << _x0 << endl;

    // cout << "x" <<endl;
    _x = _x0;
    // cout << _x << endl;
    // cout << "r" << endl;
    _r = _b - _A*_x;
    // cout << _r << endl;

    _normr = 0;

    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 100;

    VectorXd z, q, w, v, r1;
    SparseMatrix<double> L, R;
    L.resize(_N,_N);
    R.resize(_N,_N);
    z.resize(_N);
    q.resize(_N);
    w.resize(_N);

    //Résolution
       _precond->Decomposition(A);
       L = _precond->GetLeftMatrix();
       R = _precond->GetRightMatrix();


       //RESOLUTION LU Mq=r
       _precond->ResolutionLeft(L,_b);
       v = _precond->GetSolutionLU();
       _precond->ResolutionRight(R,v);
       q = _precond->GetSolutionLU();

       while ((_normr > _eps) && (k <= _kmax))
       {
         cout << "jentre dans la resolution" << endl;
         w = _A*q;
         //RESOLUTION Mz=w;
         _precond->ResolutionLeft(L,w);
         v = _precond->GetSolutionLU();
         _precond->ResolutionRight(R,v);
         z = _precond->GetSolutionLU();

         alpha = (q.dot(z)) / (z.dot(z));
         _x += alpha*q;
         _r += -alpha*w;
         q += -alpha*z;
         for (int i = 0; i < _N; i++)
           _normr += _r[i]*_r[i];
         _normr = sqrt(_normr);
         _r = r1;
         _vectnormr.push_back(_normr);
         _vectnbiteration.push_back(k);
         k++;
       }
       _nbiteration = k;

       if (k > _kmax)
       {
         cout << "Tolérance non atteinte : " << _normr << endl;
       }
     }


  //////////////////////
  //Methode 1 exo td15//
  //////////////////////
  void Meth1::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    _N = N;
    _A.resize(_N,_N);
    //Initialisation
    // cout << "N" << endl;
    _N = N;
    // cout << _N << endl;
    _A.resize(_N,_N);
    _b.resize(_N);
    // cout << "A" << endl;
    _A = A;
    // cout << _A << endl;
    _b = b;
    _r.resize(_N);
    _x.resize(_N);

    // cout << "taille x0" << endl;
    _x0.resize(_N);
    // cout << _x0.size() << endl;
    // cout << "x0" << endl;
    for (int i = 0; i < _N; i++)
      _x0[i] = i;
    // cout << _x0 << endl;

    // cout << "x" <<endl;
    _x = _x0;
    // cout << _x << endl;
    // cout << "r" << endl;
    _r = _b - _A*_x;
    // cout << _r << endl;

    _normr = 0;

    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 100;

    //Résolution
   VectorXd z, q, y, v, w;
   SparseMatrix<double> L, R, invR, invL,B;
   double alpha;
   int k(0);

   L.resize(_N,_N);
   invL.resize(_N,_N);
   R.resize(_N,_N);
   invR.resize(_N,_N);
   B.resize(_N,_N);
   z.resize(_N);
   w.resize(_N);
   q.resize(_N);
   v.resize(_N);
   y.resize(_N);

   //DECOMPOSITION DE M
   _precond->Decomposition(A);
   L = _precond->GetLeftMatrix();
   R = _precond->GetRightMatrix();

   //RESOLUTION LU
   _precond->ResolutionLeft(L,_b);
   v = _precond->GetSolutionLU();
   _precond->ResolutionRight(R,v);
   q = _precond->GetSolutionLU();

   y = L*_x0;
   _precond->ResolutionLeft(L,_b-_A*_x0);
   _r = _precond->GetSolutionLU();

   while ((_normr > _eps) && (k <= _kmax))
   {
     _precond->ResolutionRight(R,_r);
     v = _precond->GetSolutionLU();
     _precond->ResolutionLeft(L,_A*v);
     w = _precond->GetSolutionLU();

     alpha = (_r.dot(_r) / (w.dot(_r)));
     y += alpha*_r;
     _r += -alpha*w;
     for (int i = 0; i < _N; i++)
       _normr += _r[i]*_r[i];
     _normr = sqrt(_normr);
     k++;
   }
   _precond->ResolutionRight(R,y);
   _x = _precond->GetSolutionLU();

   _nbiteration = k;

   if (k > _kmax)
   {
     cout << "Tolérance non atteinte : " << _normr << endl;
   }
 }


  //////////////////////
  //Methode 2 exo td15//
  //////////////////////
  void Meth2::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    _N = N;
    _A.resize(_N,_N);
    //Initialisation
    // cout << "N" << endl;
    _N = N;
    // cout << _N << endl;
    _A.resize(_N,_N);
    _b.resize(_N);
    // cout << "A" << endl;
    _A = A;
    // cout << _A << endl;
    _b = b;
    _r.resize(_N);
    _x.resize(_N);

    // cout << "taille x0" << endl;
    _x0.resize(_N);
    // cout << _x0.size() << endl;
    // cout << "x0" << endl;
    for (int i = 0; i < _N; i++)
      _x0[i] = i;
    // cout << _x0 << endl;

    // cout << "x" <<endl;
    _x = _x0;
    // cout << _x << endl;
    // cout << "r" << endl;
    _r = _b - _A*_x;
    // cout << _r << endl;

    _normr = 0;

    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 100;

    //Résolution
    VectorXd z, q, v, w, y;
    SparseMatrix<double> L, R, M, invL,B;
    double alpha, gamma;
    int k(0);

    L.resize(_N,_N);
    invL.resize(_N,_N);
    M.resize(_N,_N);
    R.resize(_N,_N);
    B.resize(_N,_N);
    z.resize(_N);
    q.resize(_N);
    v.resize(_N);
    w.resize(_N);
    y.resize(_N);

    //DECOMPOSITION DE M
    _precond->Decomposition(A);
    L = _precond->GetLeftMatrix();
    R = _precond->GetRightMatrix();


    //RESOLUTION LU
    _precond->ResolutionLeft(L,_b);
    v = _precond->GetSolutionLU();
    _precond->ResolutionRight(R,v);
    q = _precond->GetSolutionLU();



    y = L*_x0;
    _precond->ResolutionLeft(L,_b-_A*_x0);
    _r = _precond->GetSolutionLU();

    M = L*R;

    while ((_normr > _eps) && (k <= _kmax))
    {
      _precond->ResolutionRight(R,_r);
      v = _precond->GetSolutionLU();
      _precond->ResolutionLeft(L,_A*v);
      w = _precond->GetSolutionLU();

      alpha = ((M*_r).dot(_r)) / ((M*w).dot(w));
      y += alpha*_r;
      _r += -alpha*w;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      k++;
    }
    _precond->ResolutionRight(R,y);
    _x = _precond->GetSolutionLU();

    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }
  //GetSolution
  const VectorXd & Method::GetSolution() const
  {
    return _x;
  }

  const vector<double> & Method::GetNormr() const
  {
    return _vectnormr;
  }

  const vector<int> & Method::GetNbIteration() const
  {
    return _vectnbiteration;
  }
