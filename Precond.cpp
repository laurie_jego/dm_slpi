#include "Precond.h"
#include <fstream>
#include <iostream>

using namespace Eigen;
using namespace std;

///////////////////////////
//Constructeur par défaut//
///////////////////////////
//Constructeur
Precond::Precond()
{
}

//Resolution a gauche ie Lx=b//
void Precond::ResolutionLeft(SparseMatrix<double> L, VectorXd b )
{
  _L = L;
  _b = b;

  _N = b.size();
  _L.resize(_N,_N);
  _b.resize(_N);
  _x.resize(_N);

  _x[0] = _b[0];
  for(int i = 1 ; i < _N ; i++) //Lv = b
  {
    _x[i] = _b[i];
    for(int j = 0 ; j < i ; j++)
      _x[i] += -_L.coeffRef(i,j)*_x[j];
      // v[i] = v[i] - _L(i,j)*v[j];
    _x[i] = _x[i]/_L.coeffRef(i,i);
    // v[i] = v[i] / _L(i,i);
  }
}

//Resolution a droite ie Rx=b//
void Precond::ResolutionRight(SparseMatrix<double> R, VectorXd b)
{
  _R = R;
  _b = b;

  _N = b.size();
  _R.resize(_N,_N);
  _b.resize(_N);
  _x.resize(_N);

  _x[_N-1] = _b[_N-1]/_R.coeffRef(_N-1, _N-1);
  // _x[_N-1] = v[_N-1] / _R(_N-1,_N-1);
  for(int i = _N-2 ; i >=0 ; i = i-1) //Ux=v
  {
    _x[i] = _b[i];
    for(int j = i+1 ; j < _N ; j++)
      _x[i] = _x[i] - _R.coeffRef(i,j)*_x[j];
      // _x[i] = _x[i] - _R(i,j)*_x[j];
    _x[i] = _x[i] / _R.coeffRef(i,i);
    // _x[i] = _x[i] / _R(i,i);
  }
}

//GetSolutionLU//
const VectorXd & Precond::GetSolutionLU() const
{
  return _x;
}
//GetLeftMatrix//
const SparseMatrix<double> & Precond::GetLeftMatrix() const
{
  return _L;
}

//GetRightMatrix//
const SparseMatrix<double> & Precond::GetRightMatrix() const
{
  return _R;
}

///////////////////////////
//Gauss-Seidel symetrique//
///////////////////////////
void GaussSeidelSym::Decomposition(SparseMatrix<double> A)
{
  SparseMatrix<double> D,E,F,invD;
  double alpha;
  int k(0);

  _A = A;
  _N = A.rows();
  _A.resize(_N,_N);
  _L.resize(_N,_N);
  _R.resize(_N,_N);
  D.resize(_N,_N);
  invD.resize(_N,_N);
  E.resize(_N,_N);
  F.resize(_N,_N);

  vector<Triplet<double>> tripletsA, tripletsL, tripletsR, tripletsD, tripletsinvD, tripletsE, tripletsF;



  //DECOMPOSITION LU DE M
  for(int i = 0 ; i < _N ; i++)
  {
    tripletsD.push_back({i, i, _A.coeffRef(i,i)});
    // D(i,i) = _A(i,i);
    if (D.coeffRef(i,i)!=0)
    // if (D(i,i)!=0)
      tripletsinvD.push_back({i, i, 1./D.coeffRef(i,i)});
    // invD(i,i) = 1./D(i,i);
    else
      tripletsinvD.push_back({i, i, 0.});
      // invD(i,i) = 0.;
    for(int j = 0 ; j < _N ; j++)
    {
      if (i > j)
        tripletsE.push_back({i, j, -_A.coeffRef(i, j)});
        // E(i,j) = -_A(i,j);
      if (i < j)
        tripletsF.push_back({i, j, -_A.coeffRef(i, j)});
        // F(i,j) = -_A(i,j);
    }
  }

  _L.setIdentity();
  _L += -E*invD;
  _R = D-F;

}

//////////////////////
//Cholesky incomplet//
//////////////////////
void CholeskyIncomplet::Decomposition(SparseMatrix<double> A)
{
  double alpha, s;
  int k(0);
  _A = A;

  _N = A.rows();
  _A.resize(_N,_N);
  _L.resize(_N,_N);
  _R.resize(_N,_N);

  vector<Triplet<double>> tripletsA, tripletsL, tripletsR;


  //DECOMPOSITION  DE CHOLESKY DE M
  tripletsL.push_back({0, 0, sqrt(_A.coeffRef(0,0))});
  // _L(1,1) = sqrt(_A(1,1));
  for (int i = 0 ; i < _N ; i++)
    {
      tripletsL.push_back({i, 1, _A.coeffRef(i, 1)/_L.coeffRef(1, 1)});
      // _L(i,1) = _A(i,1)/_L(1,1);
      for (int j = 0 ; j < i ; j++)
        {
          s=_A.coeffRef(i, j);
          // _L(i,j) = _A(i,j);
          for(int h = 0 ; h < j ; h++)
            {
              s += -_L.coeffRef(i, h)*_L.coeffRef(h, j);
              // _L(i,j) += - _L(i,h)*_L(h,j);
            }
            s = s/_L.coeffRef(j, j);
            tripletsL.push_back({i, j, s});
          // _L(i,j) = _L(i,j)/_L(j,j);
        }
      tripletsL.push_back({i, i, _A.coeffRef(i, i)});
      // _L(i,i) = _A(i,i);
      s=_A.coeffRef(i, i);
      for(int j = 0 ; j < i ; j++)
        {
          s+= -pow(_L.coeffRef(i,j), 2);
          // _L(i,i) += -pow(_L(i,j),2);
        }
      s = sqrt(s);
      tripletsL.push_back({i, i, s});
      // _L(i,i) = sqrt(_L(i,i));
    }

  _R = _L.transpose();
}

//////////////////////
//No preconditioneur//
//////////////////////
void NoPrecond::Decomposition(SparseMatrix<double> A)
{
  _A = A;
}
