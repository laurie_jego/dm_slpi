#include "MatrixDefinition.h"
#include "Eigen"
#include "Sparse"
#include "Dense"
#include <iostream>
#include <fstream>

using namespace Eigen;
using namespace std;

////////////////
//Constructeur//
////////////////
MatrixDefinition::MatrixDefinition()
{

}

///////////////////////////
//Matrice du laplacien 2D//
///////////////////////////
void Laplacian::Definition(int N)
{
  _N=N;
  _A.resize(_N*_N,_N*_N);

  vector<Triplet<double>> triplets;

  for(int i=0; i<_N*_N; i++)
  {
    triplets.push_back({i,i,4.}); //Diagonale
    if(i < (_N*_N-1))
      triplets.push_back({i,i+1,-1.}); //Sur-diagonale
    if (i > 0)
      triplets.push_back({i,i-1,-1.}); //Sous-diagonale
    if (i < _N*(_N-1))
      triplets.push_back({i,i+_N,-1.}); //Diagonale du bloc supérieur
    if (i > (_N-1))
      triplets.push_back({i,i-_N,-1.}); //Diagonale du bloc inférieur
    if((i > _N-1) && (i%_N == 0))
    {
      triplets.push_back({i-1,i,1.}); //Annulation de certains éléments sur la sur-diagonale
      triplets.push_back({i,i-1,1.}); //Annulation de certains éléments sur la sous-diagonale
    }
  }
  _A.setFromTriplets(triplets.begin(), triplets.end()); //Reconstruction de la matrice creuse
}

// /////////////////////
// //Matrice aléatoire//
// /////////////////////
// Random::Random(double alpha) :
// _alpha(alpha)
// {
//
// }
//
// void Random::Definition(const int N)
// {
//   _N = N;
//   MatrixXd B;
//   _D.resize(_N,_N);
//   B.resize(_N,_N);
//   //Définition de la matrice B aléatoire
//   srand(time(0));
//   for (int i = 0 ; i < B.rows() ; i++)
//   {
//     for (int j = 0 ; j < B.cols() ; j++)
//       B(i,j) = (double)rand()/(double)RAND_MAX;
//   }
//   //Définition de la matrice dense A=alpha*I+B^t*B
//   _D.setIdentity();
//   _D = _alpha*_D +B.transpose()*B;
// }

/////////////////////////
//MatrixMarket bcsstk16//
/////////////////////////
void Barrages::Definition(const int N)
{
  _N=N;
  //Ouverture du fichier dans lequel est enregistré la matrice
  ifstream flux1("bcsstk16.mtx", ios::in);

  int i,j; //i numéro de ligne, j numéro de colonne
  double a, nul1, nul2; //a élément non nul
  vector<Triplet<double>> triplets;

  if (flux1)
  {
    //Lecture de la première ligne
    string prem_ligne;
    getline(flux1, prem_ligne);
    //Lecture de la deuxième ligne
    getline(flux1, prem_ligne);
    //Récupération de la taille de la matrice
    cout << " N = " << _N << endl;

    _A.resize(_N,_N);

    //Lecture du fichier
    while (getline(flux1,prem_ligne))
    {
      flux1 >> i >> j >> a ;
      triplets.push_back({i-1,j-1,a});
      //Partie symétrique
      if (i!=j)
        triplets.push_back({j-1, i-1, a});
    }

    flux1.close();
  }
  //Reconstruction de la matrice
  _A.setFromTriplets(triplets.begin(), triplets.end());
}

//////////////////////////////////
//Matrice du problème de Poisson//
//////////////////////////////////
void Poisson::Definition(const int N) //Même schéma que précédemment
{
  ifstream flux2("nos6.mtx", ios::in);

  _N = N;
  int i,j;
  double a, nul1, nul2;

  vector<Triplet<double>> triplets;

  if (flux2)
  {
    string prem_ligne;
    getline(flux2, prem_ligne);
    getline(flux2, prem_ligne);

    cout << "N = " << _N << endl;

    _A.resize(_N,_N);

    while (getline(flux2,prem_ligne))
    {
      flux2 >> i >> j >> a ;
      triplets.push_back({i-1, j-1, a});
      if (i!=j)
        triplets.push_back({j-1, i-1, a});
    }

    flux2.close();
  }
  _A.setFromTriplets(triplets.begin(), triplets.end());
}

//////////////////////////////////////
//Matrice de l'équation de diffusion//
//////////////////////////////////////
void Diffusion::Definition(const int N) //Même schéma que précédemment
{
  _N=N;

  ifstream flux3("nos7.mtx", ios::in);

  int i,j;
  double a, nul1, nul2;
  vector<Triplet<double>> triplets;

  if (flux3)
  {
    string prem_ligne;

    getline(flux3, prem_ligne);
    getline(flux3, prem_ligne);

    cout << "N = " << _N << endl;

    _A.resize(_N,_N);

    while (getline(flux3,prem_ligne))
    {
      flux3 >> i >> j >> a ;
      triplets.push_back({i-1,j-1,a});

      if (i!=j)
        triplets.push_back({j-1, i-1, a});
    }
    flux3.close();
  }
  _A.setFromTriplets(triplets.begin(), triplets.end());
}

/////////////////////////
//Matrice Omni Coliseum//
/////////////////////////
void OmniColiseum::Definition(const int N) //Même schéma que précédemment
{
  _N=N;

  ifstream flux4("bcsstk14.mtx", ios::in);

  int i,j;
  double a, nul1, nul2;
  vector<Triplet<double>> triplets;

  if (flux4)
  {
    string prem_ligne;
    getline(flux4, prem_ligne);
    getline(flux4, prem_ligne);

    cout << "N = " << _N << endl;

    _A.resize(_N,_N);

    while (getline(flux4,prem_ligne))
    {
      flux4 >> i >> j >> a ;
      triplets.push_back({i-1,j-1,a});

      if (i!=j)
        triplets.push_back({j-1, i-1, a});
    }
    flux4.close();
  }
  _A.setFromTriplets(triplets.begin(), triplets.end());
}

/////////////////////////////
//Récupération des matrices//
/////////////////////////////

// //Pour une matrice dense
// const MatrixXd & MatrixDefinition::GetMatrixXd() const
// {
//   return _D;
// }

//Pour une matrice creuse
const SparseMatrix<double> & MatrixDefinition::GetSparseMatrix() const
{
  return _A;
}
