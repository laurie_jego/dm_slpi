#include "Method.h"
#include "cstdlib"
#include "ctime"
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace Eigen;
using namespace std;

int main()
{
  string results; // nom du fichier
  int userChoice, MatrixChoice; // choix pour les switchs
  int N; // taille des matrices
  double alpha;
  Eigen::VectorXd x, b, r, y, test;
  std::vector<int> vectnbiteration;
  std::vector<double> vectnormr;
  Eigen::SparseMatrix<double> A;
  ofstream fileout;


  ///////////////////////
  //Choix de la matrice//
  ///////////////////////

  cout << "-------------------------------------------------" << endl;
  cout << "Choix de la matrice" << endl;
  cout << "-------------------------------------------------" << endl;
  // cout << "1) Matrice aléatoire" << endl;
  cout << "2) Laplacien 2D" << endl;
  cout << "3) Matrix Market 1 - Barrages" << endl;
  cout << "4) Matrix Market 2 - Problème de Poisson" << endl;
  cout << "5) Matrix Market 3 - Equation de diffusion" << endl;
  cout << "6) Matrix Market 4 - Omni Coliseum" << endl;
  cin >> MatrixChoice ;


  MatrixDefinition* matrix_def(0); // pointeur qui pointe sur les matrices
  switch (MatrixChoice)
  {
    // //Matrice aléatoire
    // case 1:
    // {
    //   cout << "-------------------------------------------------" << endl;
    //   cout << "Valeur de N ?" << endl;
    //   cin >> N ;
    //   cout << "-------------------------------------------------" << endl;
    //   cout << "Valeur de alpha (0.1 ou 1)" << endl;
    //   cin >> alpha;
    //
    //   matrix_def = new Random(alpha);
    //   matrix_def->Definition(N);
    //   A =  matrix_def->GetMatrixXd();
    //
    // }
    // break;

    //Matrice du laplacien 2D
    case 2:
    {
      cout << "-------------------------------------------------" << endl;
      cout << "Valeur de N ?" << endl;
      cin >> N ;

      matrix_def = new Laplacian();

    }
    break;

    //Matrices de MatrixMarket
    case 3:
    {
      N=4884;
      matrix_def = new Barrages();
      matrix_def->Definition(N);
      A = matrix_def->GetSparseMatrix();

    }
    break;

    case 4:
    {
      N=675;
      A.resize(N,N);
      matrix_def = new Poisson();
      matrix_def->Definition(N);
      A = matrix_def->GetSparseMatrix();
    }
    break;

    case 5:
    {
      N=729;
      matrix_def = new Diffusion();
      matrix_def->Definition(N);
      A = matrix_def->GetSparseMatrix();
    }
    break;

    case 6:
    {
      N=1806;
      matrix_def = new OmniColiseum();
      matrix_def->Definition(N);
      A = matrix_def->GetSparseMatrix();
    }
    break;

    default:
    cout << "Pas bon chiffre MAGGLE" << endl;
    exit(0);
  }


  b.resize(N);
  test.resize(N);
  x.resize(N);

  //Définition du vecteur b

    /////////////////////////////////////
    //Choix de la méthode de résolution//
    /////////////////////////////////////

  cout << "-------------------------------------------------" << endl;
  cout << "Choix de la méthode" << endl;
  cout << "-------------------------------------------------" << endl;
  cout << "1) Méthode du gradient à pas optimal" << endl;
  cout << "2) Méthode de résidu minimum" << endl;
  cout << "3) Méthode du gradient conjugué" << endl;
  cin >> userChoice;


  //Poiteur qui pointe sur toutes les méthodes
  Method* meth(0);

  switch(userChoice)
  {
    //Gradient à pas otpimal
    case 1:
    meth = new GradientPasOptimal();
    results ="gpo.txt";
    cout << "-------------------------------------------------" << endl;
    cout << "Le fichier créé s'appelle gpo.txt" << endl;
    cout << "-------------------------------------------------" << endl;

    break;

    //Résidu minimum
    case 2:
    meth = new ResiduMinimum();
    results ="rm.txt";
    cout << "-------------------------------------------------" << endl;
    cout << "Le fichier créé s'appelle rm.txt" << endl;
    cout << "-------------------------------------------------" << endl;

    break;

    //Gradient conjugué
    case 3:
    meth = new GradientConjugue();
    results ="gc.txt";
    cout << "-------------------------------------------------" << endl;
    cout << "Le fichier créé s'appelle gc.txt" << endl;
    cout << "-------------------------------------------------" << endl;

    break;

    //Résidu minimum préconditionné à gauche
    case 4:
    meth = new ResMinPG();
    results = "rmpg.txt";
    cout << "-------------------------------------------------" << endl;
    cout << "Le fichier créé s'appelle rmpg.txt" << endl;
    cout << "-------------------------------------------------" << endl;

    default:
    cout << "Nan mais 1, 2 ou 3 c'est pas compliqué bouffon va ! Tu sais pas lire ma parole !! Allez recommence tiens !" << endl;
    exit(0);
  }

  // vectnormr.resize(4*N);
  // vectnbiteration.resize(N);
  fileout.open(results);
  if (MatrixChoice == 2)
  {
    b.resize(N*N);
      for (int j = 0 ; j < N*N ; j++)
        b[j] = 1;


      matrix_def->Definition(N);
      A = matrix_def->GetSparseMatrix();

      meth->Resolution(b, A, N*N);

      vectnormr=meth->GetNormr();
      // cout << "getnbiter" << endl;
      // vectnbiteration=meth->GetNbIteration();
      for (int i=0; i<vectnormr.size(); i++)
        fileout << vectnormr[i] << endl;
    }
    else
    {
      for (int j = 0 ; j < N ; j++)
        b[j] = 1;
      matrix_def->Definition(N);
      A = matrix_def->GetSparseMatrix();

      cout << "res" << endl;
      meth->Resolution(b, A, N);
      cout << "getnorm" << endl;
      vectnormr = meth->GetNormr();

      cout << "file" << endl;
      for (int i=0; i<vectnormr.size(); i++)
        fileout << vectnormr[i] << endl;
    }

    // for(int i=0; i<N; i++){
    // cout << "normr = " << vectnormr[i] << endl;
    // cout << "nbiteration" << vectnbiteration[i] << endl;}

  // cout << vectnormr << endl;
  // cout << vectnbiteration << endl;
  // cout << "erreur" << endl;



  // meth->Resolution(b, A, N);

  // meth->SaveSolution(results);

  // cout << "Taille de x = " << x.size() <<endl;
  // cout << "Taille de b = " << b.size() << endl;

  // y = meth->GetSolution();

  delete meth;

  return 0;
}
