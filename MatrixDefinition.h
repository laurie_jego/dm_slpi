#include "Eigen"
#include "Sparse"
#include "Dense"
#include <fstream>
#include <iostream>

class MatrixDefinition
{
protected:
  int _N;
  Eigen::SparseMatrix<double> _A;
  // Eigen::MatrixXd _D;


public:
  //Constructeur
  MatrixDefinition();
  //Définition des matrices
  virtual void Definition(const int _N)=0;
  //Récupération des matrices denses
  const Eigen::MatrixXd & GetMatrixXd() const;
  //R2cupération des matrices creuses
  const Eigen::SparseMatrix<double> & GetSparseMatrix() const;

};

//Matrice Laplacien
class Laplacian : public MatrixDefinition
{
public:
  void Definition(const int _N);

};

// //Matrice aléatoire
// class Random : public MatrixDefinition
// {
// private:
//   double _alpha;
//
// public:
//   Random(double alpha);
//   void Definition(const int _N);
// };

//MatrixMarket

class Barrages : public MatrixDefinition
{
public:
  void Definition(const int _N);

};

class Poisson : public MatrixDefinition
{
public:
  void Definition(const int _N);

};

class Diffusion : public MatrixDefinition
{
public:
  void Definition(const int _N);

};

class OmniColiseum : public MatrixDefinition
{
public:
  void Definition(const int _N);

};
