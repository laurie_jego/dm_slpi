#include "dm.h"
#include <iostream>
#include <fstream>

using namespace Eigen;
using namespace std;


///////////////////////////
//Constructeur par défaut//
///////////////////////////
//Constructeur
Method::Method(): _precond(0)
{

}
//SaveSolution
void Method::SaveSolution(const std::string file_name)
{
  _fileout.open(file_name);
  for (int i = 0; i < _x.size(); i++)
  {
    _fileout << _x[i] << endl;;}
  }

  //GetSolution
  const VectorXd & Method::GetSolution() const
  {
    return _x;
  }

  const double & Method::GetNormr() const
  {
    return _normr;
  }

  const int & Method::GetNbIteration() const
  {
    return _nbiteration;
  }

  //Choix preconditionneur
void Method::PrecondChoice(Precond* precond)
{
  _precond = precond;
}


  //////////////////////////
  //Gradient à pas optimal//
  //////////////////////////
  void GradientPasOptimal::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    //Initialisation
    _b = b;
    _A = A;
    _N = _b.size();
    _A.resize(_N,_N);
    _x0.resize(_N);
    for (int i = 0; i < _N; i++)
    {_x0[i] = i;}
    _x = _x0;
    _r = _b - A*_x;
    _normr = 0;
    for (int i = 0; i < _N; i++)
    _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);
    _eps = 0.000001;
    _kmax = 100;

    //Résolution
    VectorXd z;
    z.resize(_N);
    double alpha;
    int k(0);

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = A*_r;
      alpha = (_r.dot(_r)) / (z.dot(_r));
      _x += alpha*_r;
      _r += -alpha*z;
      _normr = 0.;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      k++;
    }
    _nbiteration = k;
    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << "  " << k << endl;
    }

  }

  //////////////////
  //Résidu minimum//
  //////////////////
  void ResiduMinimum::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    //Initialisation
    _b = b;
    _A = A;
    _N = _b.size();
    _A.resize(_N,_N);
    _b.resize(_N);
    _x0.resize(_N);
    for (int i = 0; i < _N; i++)
      _x0[i] = 0.;
    _x = _x0;
    _r = _b - _A*_x;
    _normr = 0;
    for (int i = 0; i < _N; i++)
    _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);
    _eps = 0.00001;
    _kmax = 1000;

    //Résolution
    VectorXd z;
    z.resize(_N);
    double alpha;
    int k(0);

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = _A*_r;
      alpha = (_r.dot(z)) / (z.dot(z));
      _x += alpha*_r;
      _r += -alpha*z;
      _normr = 0.;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }

  // /////////////////////
  // //Gradient conjugué//
  // /////////////////////
  void GradientConjugue::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    //Initialisation
    _b = b;
    _A=A;
    _A.resize(_N,_N);
    _N = _b.size();
    _x0.resize(_N);
    for (int i = 0; i < _N; i++)
      _x0[i] = 0.;
    _x = _x0;
    _r = _b - _A*_x;
    _normr = 0;
    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);
    _eps = 0.000001;
    _kmax = 100;

    //Résolution
    VectorXd p, z, r1;
    double alpha, gamma;
    int k(0);
    p = _r;

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = _A*p;
      alpha = (_r.dot(_r)) / (z.dot(p));
      _x = _x + alpha*p;
      r1 = _r - alpha*z;
      gamma = (r1.dot(r1)) / (_r.dot(_r));
      p = r1 + gamma*p;
      _normr = 0.;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      _r = r1;
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }

  //////////////////////////////////////////
  //Résidu minimum preconditionne a gauche//
  //////////////////////////////////////////
  void ResMinPG::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    double alpha;
    int k(0);
    //Initialisation
    _b = b;
    _A = A;
    _N = _b.size();
    _x0.resize(_N);
    _A.resize(_N,_N);
    for (int i = 0; i < _N; i++)
      _x0[i] = 0.;
    _x = _x0;
    _r = _b - _A*_x;
    _normr = 0;
    for (int i = 0; i < _N; i++)
      _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);
    _eps = 0.000001;
    _kmax = 100;

    VectorXd z, q, w;
    SparseMatrix<double> L, R;
    L.resize(_N,_N);
    R.resize(_N,_N);
    z.resize(_N);
    q.resize(_N);
    w.resize(_N);

    //Résolution
    cout << "erreurcpp1" << endl;
    _precond->Decomposition(A);
    cout << "erreurcpp2" << endl;
    L = _precond->GetLeftMatrix();
    R = _precond->GetRightMatrix();


    //RESOLUTION LU Mq=r
    _precond->ResolutionLU(L,R,_b);
    q = _precond->GetSolutionLU();

    while ((_normr > _eps) && (k <= _kmax))
    {
      cout << "jentre dans la resolution" << endl;
      w = _A*q;
      //RESOLUTION Mz=w;
      _precond->ResolutionLU(L,R,w);
      z = _precond->GetSolutionLU();

      alpha = (q.dot(z)) / (z.dot(z));
      _x += alpha*q;
      _r += -alpha*w;
      q += -alpha*z;
      _normr = 0.;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }


  //////////////////////
  //Methode 1 exo td15//
  //////////////////////
  void Meth1::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    _N = N;
    _A.resize(_N,_N);
    //Initialisation
    _b = b;
    _A = A;


    _x0.resize(_N);
    for (int i = 0; i < _N; i++)
    _x0[i] = 0.;

    _x = _x0;

    _normr = 0;

    for (int i = 0; i < _N; i++)
    _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 100;

    //Résolution
    VectorXd z, q, v, w;
    SparseMatrix<double> L, R, invD, invL,B;
    double alpha;
    int k(0);

    L.resize(_N,_N);
    invL.resize(_N,_N);
    R.resize(_N,_N);
    B.resize(_N,_N);
    z.resize(_N);
    q.resize(_N);
    v.resize(_N);
    w.resize(_N);

    //DECOMPOSITION LLt DE M: Cholesky
    _precond->Decomposition(A);
    L = _precond->GetLeftMatrix();
    R = _precond->GetRightMatrix();


    //RESOLUTION LU Mq=r
    _precond->ResolutionLU(L,R,_b);
    q = _precond->GetSolutionLU();
    //calcul inverse L
    //

    B = invL*_A*invL.transpose();

    _r = invL*_b - B*_x;

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = B*_r;
      alpha = (_r.dot(_r) / (z.dot(_r)));
      _x += alpha*_r;
      _r += -alpha*z;
      _normr = 0.;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }

  //////////////////////
  //Methode 2 exo td15//
  //////////////////////
  void Meth2::Resolution(VectorXd b, SparseMatrix<double> A, int N)
  {
    _N = N;
    _A.resize(_N,_N);
    //Initialisation
    _b = b;
    _A = A;


    _x0.resize(_N);
    for (int i = 0; i < _N; i++)
    _x0[i] = 0.;

    _x = _x0;

    _normr = 0;

    for (int i = 0; i < _N; i++)
    _normr += _r[i]*_r[i];

    _normr = sqrt(_normr);

    _eps = 0.000001;
    _kmax = 100;

    //Résolution
    VectorXd z, q, v, w;
    SparseMatrix<double> L, R, invD, invL,B;
    double alpha;
    int k(0);

    L.resize(_N,_N);
    invL.resize(_N,_N);
    R.resize(_N,_N);
    B.resize(_N,_N);
    z.resize(_N);
    q.resize(_N);
    v.resize(_N);
    w.resize(_N);

    //DECOMPOSITION LLt DE M: Cholesky
    _precond->Decomposition(A);
    L = _precond->GetLeftMatrix();
    R = _precond->GetRightMatrix();


    //RESOLUTION LU Mq=r
    _precond->ResolutionLU(L,R,_b);
    q = _precond->GetSolutionLU();

    //calcul inverse L
    //

    B = _A;//B= inv(M)

    _r = _b - _A*_x; //pe faux

    while ((_normr > _eps) && (k <= _kmax))
    {
      z = B*_r;
      alpha = (z.dot(z)) / ((_A*z).dot(z));//a changer ouaish
      _x += alpha*_r;
      _r += -alpha*z;
      _normr = 0.;
      for (int i = 0; i < _N; i++)
        _normr += _r[i]*_r[i];
      _normr = sqrt(_normr);
      k++;
    }
    _nbiteration = k;

    if (k > _kmax)
    {
      cout << "Tolérance non atteinte : " << _normr << endl;
    }
  }
