#include "Eigen"
#include "Sparse"
#include "Dense"
#include "Precond.h"
#include "MatrixDefinition.h"
#include <fstream>
#include <iostream>

class Method
{
protected:
  Eigen::VectorXd _r, _b, _x, _x0;
  // Eigen::MatrixXd _A;
  Eigen::SparseMatrix<double> _A;
  double _normr, _eps;
  int _kmax, _N, _nbiteration;
  Precond * _precond;
  MatrixDefinition* _matrixdef;


private:
  std::ofstream _fileout;

public:
  Method();
  // Initialiser le nom du fichier solution
  //void InitializeFileName(const std::string file_name);

  virtual void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N)=0;
  void PrecondChoice(Precond * precond);
  void SaveSolution(const std::string file_name);
  const Eigen::VectorXd &  GetSolution() const;
  const double & GetNormr() const;
  const int & GetNbIteration() const;

};


//////////////////////////
//Gradient à pas optimal//
//////////////////////////
class GradientPasOptimal : public Method
{
public:
  void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N);
};

//////////////////
//Résidu minimum//
//////////////////
class ResiduMinimum : public Method
{
public:
  void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N);
};

/////////////////////
//Gradient conjugué//
/////////////////////
 class GradientConjugue : public Method
 {
 public:
   void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N);
 };

 //////////////////////////////////////////
 //Residu minimum preconditionne a gauche//
 //////////////////////////////////////////
  class ResMinPG : public Method
  {
  public:
    void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N);
  };

  //////////////////////
  //Methode 1 ex 15 td//
  //////////////////////
   class Meth1 : public Method
   {
   public:
     void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N);
   };

   class Meth2 : public Method
   {
   public:
     void Resolution(Eigen::VectorXd b, Eigen::SparseMatrix<double> A, int N);
   };
